# README #

This repository is a playground for the code examples found in the book "Unit Test Frameworks - Tools for High-Quality Software Development"

### Organization ###

The repository is organized in folders, one for each chapter of the book. Each folder may have one or more subfolders containing Eclipse Java projects.

