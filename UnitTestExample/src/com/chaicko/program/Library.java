package com.chaicko.program;

public class Library {
	
	private Book book;
	
	public Library() {
	}

	public void addBook(Book expectedBook) {
		this.book = expectedBook;
	}

	public Book getBook(String string) {
		return this.book;
	}
}
