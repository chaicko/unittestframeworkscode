package com.chaicko.test;

public class TestRunner {

	public static void main(String[] args) {
		TestRunner tester = new TestRunner();
		tester.runTest();
	}
	
	private UnitTest test;
	private UnitTest test2;
	
	public TestRunner() {
		test = new BookTest();
		test2 = new LibraryTest();
	}
	
	private void runTest() {
		try {
			test.runTest();
			test2.runTest();
			System.out.println("SUCCESS!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("FAILURE!");
		}
		System.out.println(UnitTest.getNumSuccess() + " tests completed successfully");
	}
}
