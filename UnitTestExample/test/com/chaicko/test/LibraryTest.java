package com.chaicko.test;

import com.chaicko.program.Book;
import com.chaicko.program.Library;

public class LibraryTest extends UnitTest {
	
	private void testAddOneBook() throws Exception {
		Library library = new Library();
		Book expectedBook = new Book("Dune");
		library.addBook( expectedBook );
		Book actualBook = library.getBook("Dune");
		assertTrue(actualBook.title.equals("Dune"), "got book");
	}

	@Override
	public void runTest() throws Exception {
		testAddOneBook();
	}

}
